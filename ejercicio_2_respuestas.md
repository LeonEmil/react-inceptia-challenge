Respuestas a las preguntas del ejercicio 2

2.1) ¿Cómo implementarías las acciones del frontend utilizando redux? (por
ejemplo autenticación, solicitud de clientes activos para el usuario y
solicitud de casos por cliente)
Respuesta: Crearía un store para guardar los datos del usuario, clientes y casos junto con los actionCreators necesarios para poder guardar y solicitar esos datos y tenerlos disponibles en donde se necesite en la aplicación. Para esto usaría redux junto con react-redux.

2.2) Si quisiéramos agregar una ruta nueva a la app, ¿cómo reestructurarías
el index.js?
Respuesta: En realidad preferiría reestructurar el archivo App.js para agregar rutas, importar los componentes y crear una carpeta para ordenar los componentes de esas rutas con react router o alguna herramienta similar. 