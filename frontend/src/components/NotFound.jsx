import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = () => {
    return (
        <div className="not-found">
            <h1 className="not-found__title">Página no encontrada</h1>
            <Link to="/">Volver al inicio</Link>
        </div>
    )
}

export default NotFound