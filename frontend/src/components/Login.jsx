import React, { useState } from 'react';
import { Navigate } from 'react-router-dom'
import axios from 'axios'
import { useSessionStorage } from './HOCS/useSessionStorage'

const Login = () => {

    const [redirect, setRedirect] = useState(false)
    const [user, setUser] = useSessionStorage('userIC', undefined)
    const [token, setToken] = useSessionStorage('tokenIC', undefined)

    const handleSubmit = (e) => {
        e.persist()
        e.preventDefault()
        axios.post('https://admindev.inceptia.ai/api/v1/login/', {
            "email": e.target.email.value,
            "password": e.target.password.value
        })
        .then( async (response) => {
            await setUser({
                email: e.target.email.value,
                password: e.target.password.value
            })
            await setToken(response.data.token)
            setRedirect(true)
        })
        .catch(error => {
            alert(error.message)
        })
    }

    return redirect ? (<Navigate replace to="/" />) : (
        <div className="login">
            <h1 className="login__title">Ingresar</h1>
            <form className="login__form" onSubmit={handleSubmit}>

                <div className="login__group">
                    <label htmlFor="email" className="login__label">Email</label>
                    <input
                        type="email"
                        className="login__input"
                        id="login-email"
                        name="email"
                        required
                    />
                </div>

                <div className="login__group">
                    <label htmlFor="password" className="login__label">Contraseña</label>
                    <input
                        type="password"
                        className="login__input"
                        id="login-password"
                        name="password"
                        required
                    />
                </div>

                <input
                    type="submit"
                    value="Continuar"
                    className="login__button"
                />
            </form>
        </div>
    );
}

export default Login;