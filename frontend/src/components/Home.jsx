import React, { useState, useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import { useSessionStorage } from './HOCS/useSessionStorage'
import axios from 'axios'

const Home = () => {

    const [user] = useSessionStorage('userIC', undefined)
    const [token] = useSessionStorage('tokenIC', undefined)
    const [clients, setClients] = useState([])
    const [startDate, setStartDate] = useState("2021-09-01");
    const [endDate, setEndDate] = useState("2022-04-20");
    const [cases, setCases] = useState([])
    const [selectedClient, setSelectedClient] = useState()
    const [inputSearch, setInputSearch] = useState("")
    const [caseCategory, setCaseCategory] = useState("Todos")

    useEffect(() => {
        const interval = setInterval(() => {
            axios.get('https://admindev.inceptia.ai/api/v1/clients/', {
                headers: {
                    'Authorization': `JWT ${token}`,
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                console.log(response)
                let newClients = []
                response.data.forEach((client) => {
                    newClients.push(client)
                })
                setClients(newClients)
            })
            .catch(error => {
                console.log(error)
            })
            
            if(selectedClient){
                console.log(`https://admindev.inceptia.ai/api/v1/inbound-case/?client=${selectedClient}&local_updated__date__gte=${startDate}&local_updated__date__lte=${endDate}`)
                axios.get(`https://admindev.inceptia.ai/api/v1/inbound-case/?client=${selectedClient}&local_updated__date__gte=${startDate}&local_updated__date__lte=${endDate}`, {
                    headers: {
                        'Authorization': `JWT ${token}`,
                        'Content-Type': 'application/json'
                    }
                })
                .then(response => {
                    let newCases = []
                    response.data.results.forEach((el) => {
                        newCases.push(el)
                    })
                    setCases(newCases)
                })
                .catch(error => {
                    console.log(error)
                })
            } 
        }, 1500)
        return () => clearInterval(interval)
    }, [token, selectedClient, cases, endDate, startDate])

    // Verifica el caso del cliente con la búsqueda y categoría seleccionada
    const verifyCase = (inputSearch, caseCategory, caseData) => {
        if(inputSearch === "" && caseCategory === "Todos") return true
        if(inputSearch === "" && caseCategory === caseData["case_result"].name) return true
        if((inputSearch !== "" && ( (caseData.id.toString().startsWith(inputSearch)) || (caseData.phone.toString().startsWith(inputSearch)) || (caseData["extra_metadata"].dni.toString().startsWith(inputSearch)) )) && caseCategory === "Todos") return true
        if((inputSearch !== "" && ( (caseData.id.toString().startsWith(inputSearch)) || (caseData.phone.toString().startsWith(inputSearch)) || (caseData["extra_metadata"].dni.toString().startsWith(inputSearch)) )) && caseCategory === caseData["case_result"].name) return true
        return false
    }

    return user ? (
        <div className="home">
            <div className="clients">
                <h2 className="clients__title">Cliente</h2>
                <ul className="clients__ul">
                    {
                        clients.map((client, key) => {
                            return <li 
                                key={key} 
                                onClick={() => { setSelectedClient(client.id) }} 
                                className={selectedClient === client.id ? "clients__li clients__li--selected" : "clients__li"}
                                >
                                    {client.name}
                                </li>
                        })
                    }
                </ul>
            </div>
            <header className="header">
                <h1 className="header__title">Reportes</h1>
                <div className="header__search">
                    <label className="header__search-label sr-only sr-only-focusable">Buscar por ID caso, ID cliente o teléfono</label>
                    <input type="search" placeholder="Buscar por ID caso, ID cliente o teléfono..." className="header__search-input" onChange={(e) => {setInputSearch(e.target.value)}}></input>
                </div>
            </header>
            <nav className="menu">
                <div className="menu__selection">
                <ul className="menu__selection-ul">
                    <li className="menu__selection-li">Detalle</li>
                </ul>
                </div>
                <div className="menu__time">
                    <input type="date" value={startDate} onChange={(e) => { setStartDate(e.target.value) }} />
                    <input type="date" value={endDate} onChange={(e) => { setEndDate(e.target.value) }} />
                </div>
            </nav>
            <div className="table-category">
                <ul className="table-category__ul">
                    <li className={caseCategory === "Todos" ? "table-category__li table-category__li--selected" : "table-category__li"} onClick={() => {setCaseCategory("Todos")}}>Todos</li>
                    <li className={caseCategory === "Transferido" ? "table-category__li table-category__li--selected" : "table-category__li"} onClick={() => {setCaseCategory("Transferido")}}>Transferido</li>
                    <li className={caseCategory === "Niega confirmación de datos" ? "table-category__li table-category__li--selected" : "table-category__li"} onClick={() => {setCaseCategory("Niega confirmación de datos")}}>Niega confirmación de datos</li>
                    <li className={caseCategory === "Cliente no encontrado en DB" ? "table-category__li table-category__li--selected" : "table-category__li"} onClick={() => {setCaseCategory("Cliente no encontrado en DB")}}>Cliente no encontrado en DB</li>
                    <li className={caseCategory === "Llamando" ? "table-category__li table-category__li--selected" : "table-category__li"} onClick={() => {setCaseCategory("Llamando")}}>Llamando</li>
                    <li className={caseCategory === "Cortó cliente" ? "table-category__li table-category__li--selected" : "table-category__li"} onClick={() => {setCaseCategory("Cortó cliente")}}>Cortó cliente</li>
                    <li className={caseCategory === "Mail enviado" ? "table-category__li table-category__li--selected" : "table-category__li"} onClick={() => {setCaseCategory("Mail enviado")}}>Mail enviado</li>
                    <li className={caseCategory === "Indefinido" ? "table-category__li table-category__li--selected" : "table-category__li"} onClick={() => {setCaseCategory("Indefinido")}}>Indefinido</li>
                </ul>
            </div>
            {
                selectedClient ? 
                <table className="table">
                    <thead className="table__header">
                        <tr>
                            <th className="table__header-item">Gestionado</th>
                            <th className="table__header-item">ID caso</th>
                            <th className="table__header-item">Teléfono</th>
                            <th className="table__header-item">DNI</th>
                            <th className="table__header-item">Grupo</th>
                            <th className="table__header-item">Orden</th>
                            <th className="table__header-item">Llamada</th>
                            <th className="table__header-item">Estado</th>
                        </tr>
                    </thead>
                    <tbody className="table__body">
                    {
                        cases.map((el, key) => (
                            verifyCase(inputSearch, caseCategory, el) ?
                            <tr key={key} className="table__body-row">
                                <td className="table__body-data">{el["last_updated"]}</td>
                                <td className="table__body-data">{el.id}</td>
                                <td className="table__body-data">{el.phone}</td>
                                <td className="table__body-data">{el["extra_metadata"].dni}</td>
                                <td className="table__body-data">{el["extra_metadata"].grupo}</td>
                                <td className="table__body-data">{el["extra_metadata"].orden}</td>
                                <td className="table__body-data">{el["case_duration"]}</td>
                                <td className="table__body-data">{el["case_result"].name}</td>
                            </tr>
                            : null
                        ))
                    }
                    </tbody>
                </table>
                : <p className="table__message">Seleccione un cliente por favor</p>
            }
        </div>
    )
    :
    <Navigate replace to="/ingresar" />
}

export default Home